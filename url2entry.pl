#!/usr/bin/env perl
#===============================================================================
#
#         FILE: url2entry.pl
#
#        USAGE: ./url2entry.pl  
#
#  DESCRIPTION: Convert url list from telegram into shaarli entry with 
#               content description. Skip personal/non-covid links.
#      OPTIONS: urlfile, shaarli_json_file
# 
# REQUIREMENTS: Image::Grab WWW::Mechanize HTML::TreeBuilder::XPath XML::FEED
#               JSON Data::Dumper
#       AUTHOR: Martin Leyrer <leyrer@gmail.com>
# ORGANIZATION: Organized? Who, me?
#      VERSION: 1.0
#      CREATED: 2020-ß3-06
#     REVISION: Telegraph Road
#===============================================================================

use strict;
use warnings;
use utf8;
use Data::Dumper;
use HTML::TreeBuilder::XPath;
use WWW::Mechanize;
use XML::Feed;
use Image::Grab qw(grab);
use REST::Client;
use JSON;
use MIME::Base64;

my $DEBUG_META = 0;
my $wfile = $ARGV[0];
my $file = $ARGV[1];
my $jsonfile = $ARGV[2];
my @urls;
my %hosts = qw();
my %okurls = qw();
my @skipstring = qw();

# Read in "OKurls" - URLs to work on and URLs to skip (@skipstring)
open( WORK, $wfile) or die "Error reading $wfile. $!\n";
while( my $line = <WORK>)  {   
	chomp $line;
	$line =~ s/^([-+]) +/$1\t/gi;	# for editors that replace tab with space
	my @data = split(/\t/,$line);
	# print ">" . $data[0] . "|" . $data[1] . "<\n";
	if( $data[0] eq '-') {
		my $tt = (not defined $data[1]) ? "XXX" : $data[1];
		$tt = lc($tt);
		push(@skipstring, $tt);
	} else {
		my $tt = (not defined $data[1]) ? "XXX" : $data[1];
		$tt = lc($tt);
		$okurls{$tt} = 1;
	}
}
close(WORK);

# slurp in URLS and remove links containing words to skip
open( IN, $file) or die "Error reading $file. $!\n";
my $url = <IN>;
chomp $url;
FETCHURL: while( my $url= <IN>)  {   
	# print STDERR $line;
	chomp $url;
	$url =~ /href\s*=\s*\"(.*?)\"?\s*$/i;
	my $t = $1;
	foreach my $x (@skipstring) {
		if( $t =~ /$x/i ) {
			next FETCHURL;
		}
	}
	push(@urls, $1);
}
close(IN);

foreach my $u (@urls) {
	$u =~ /^https?:\/\/(.+?)(\/|$)/i;
	my $host = $1;
	die "No host for $u!!!\n" if(not defined $host);
	next if( not $okurls{$host} );
	# next if( not $u =~/derstandard\.at/i); 
	# next if( not $u =~/nbcnews\.com/i); 
	# print "$u\n";
	parse_site($u, "x", "y");
}

sub parse_site{
	my($url, $headline, $eintrag) = @_;
	my $tree= HTML::TreeBuilder::XPath->new;
	my $title = "";
	my $desc = "";
	my $date = "";

	my $mech=WWW::Mechanize->new(agent => 'Telegram2Shaarli', autocheck => 0);
	my $resp = $mech->get($url);
	if( not $resp->is_success() ) {
		warn "Skipping $url\n";
		return();
	}
	$tree->parse($mech->content(decoded_by_headers => 1)); 
	
	# parse article for "article::published_time" meta data
	$date = meta_article_date($tree, $date);

	my $meta_og = $tree->findnodes( '//meta[contains(@property, "og:")]'); 
	if( $meta_og->size() > 0) {
		foreach my $node ($meta_og->get_nodelist()) {
			my $name = $node->attr('property');
			my $value = $node->attr('content');
			#print "$name - $value \n";
			print "$name: $value<\n" if( $DEBUG_META );
			if( $name eq 'og:image' and $date eq '' ) {
				$date = date_og_image($value);
			} elsif( $name eq 'og:title' and $title eq '' ) {
				$title = $value;
			} elsif( $name eq 'og:description' and $desc eq '' ) {
				$desc = $value;
			}
		}
	}

	my $meta_krone= $tree->findnodes( '//meta[contains(@name, "krn-")]'); 
	if( $meta_krone->size() > 0) {
		foreach my $node ($meta_krone->get_nodelist()) {
			my $name = $node->attr('name');
			my $value = $node->attr('content');
			print "$name: $value<\n" if( $DEBUG_META );
			if( $name eq 'krn-article-modified' and $date eq '' ) {
				$date = $value;
			}
		}
	}

	my $meta_heise = $tree->findnodes( '//meta[contains(@name, "date")]'); 
	if( $meta_heise->size() > 0) {
		foreach my $node ($meta_heise->get_nodelist()) {
			my $name = $node->attr('name');
			my $value = $node->attr('content');
			print "$name: $value<\n" if( $DEBUG_META );
			if( $name eq 'date' and $date eq '' ) {
				$date = $value;
			}
		}
	}

	my $meta_orf= $tree->findnodes( '//meta[contains(@name, "dc.")]'); 
	if( $meta_orf->size() > 0) {
		foreach my $node ($meta_orf->get_nodelist()) {
			my $name = $node->attr('name');
			my $value = $node->attr('content');
			print "$name: $value<\n" if( $DEBUG_META );
			if( $name eq 'dc.date' and $date eq '' ) {
				$date = $value;
			}
		}
	}

	my $meta_bento= $tree->findnodes( '//meta[contains(@name, "description")]'); 
	if( $meta_bento->size() > 0) {
		foreach my $node ($meta_bento->get_nodelist()) {
			my $name = $node->attr('name');
			my $value = $node->attr('content');
			print "$name: $value<\n" if( $DEBUG_META );
			if( $name eq 'description' and $desc eq '' ) {
				$desc = $value;
			}
		}
	}

	if( $desc eq '' and $url =~ /orf\.at/i ) {
		$desc = $tree->findnodes(  '//div[@class="story-story"]' )->[0]->as_text();
	}
	if( $desc eq '' and $url =~ /derstandard\.at/i ) {
		$desc = $tree->findnodes(  '//div[@class="article-text"]' )->[0]->as_text();
	}
	if( $title eq '' and $url =~ /derstandard\.at/i ) {
		$title = $tree->findnodes( '//h1')->[0]->as_text();
	}
	
	if( $date eq '' and $url =~ /nbcnews\.com/i ) {
		my $t = $mech->content(decoded_by_headers => 1); 
		if( $t =~ /currentMessageId.*?updatedDate\":\"(.*?\(UTC\))/imsg ) {
			$date = $1;
		}
	}

	# If everything else fails ...
	if( $title eq '') {
		$title = $tree->findnodes( '//title' )->[0]->as_text(); 
	}

	if( $title eq 'news.ORF.at' ) {
		warn "Text no longer available. Skipping $url\n";
		return();
	}

	# Cleanup
	$title =~ s/ - DER SPIEGEL.*$//gi;

	if( $title eq '' or $desc eq '' or $date eq '') {
		print "$url\nDa fehlt was!\n";
		print "Title: $title\nDesc: $desc\nDate: $date\nURL: $url\n\n";
		my $t = $mech->content(decoded_by_headers => 1); 
		$t =~ s/\</\n</gi;
		print "$t" if($DEBUG_META);
		exit(1);
	} else {
		print "OK:\n$title\n$desc\n$date\n$url\n\n";
	}

	# exit(2);
}



# parse article for "article::published_time" meta data
sub meta_article_date {
	my ($tree, $date) = @_; 
	my $ret = '';

	my $meta_article = $tree->findnodes( '//meta[contains(@property, "article:")]'); 
	if( $meta_article->size() > 0) {
		foreach my $node ($meta_article->get_nodelist()) {
			my $name = $node->attr('property');
			my $value = $node->attr('content');
			print "$name: $value<\n" if( $DEBUG_META );
			if( $name eq 'article:published_time' and $date eq '' ) {
				$ret = $value;
			}
		}
	}
	return ( ($ret ne '') ? $ret : $date );
}

# Prep for refactoring. ignore now START
#sub meta_og_image {
#	my $meta_og = $tree->findnodes( '//meta[contains(@property, "og:")]'); 
#	if( $meta_og->size() > 0) {
#		foreach my $node ($meta_og->get_nodelist()) {
#			my $name = $node->attr('property');
#			my $value = $node->attr('content');
#			#print "$name - $value \n";
#			print "$name: $value<\n" if( $DEBUG_META );
#			if( $name eq 'og:image' and $date eq '' ) {
#				$date = date_og_image($value);
#			} elsif( $name eq 'og:title' and $title eq '' ) {
#				$title = $value;
#			} elsif( $name eq 'og:description' and $desc eq '' ) {
#				$desc = $value;
#			}
#		}
#	}
#}

#sub meta_og_title
#	my $meta_og = $tree->findnodes( '//meta[contains(@property, "og:")]'); 
#	if( $meta_og->size() > 0) {
#		foreach my $node ($meta_og->get_nodelist()) {
#			my $name = $node->attr('property');
#			my $value = $node->attr('content');
#			#print "$name - $value \n";
#			print "$name: $value<\n" if( $DEBUG_META );
#			if( $name eq 'og:image' and $date eq '' ) {
#				$date = date_og_image($value);
#			} elsif( $name eq 'og:title' and $title eq '' ) {
#				$title = $value;
#			} elsif( $name eq 'og:description' and $desc eq '' ) {
#				$desc = $value;
#			}
#		}
#	}

#sub meta_og_description
#	my $meta_og = $tree->findnodes( '//meta[contains(@property, "og:")]'); 
#	if( $meta_og->size() > 0) {
#		foreach my $node ($meta_og->get_nodelist()) {
#			my $name = $node->attr('property');
#			my $value = $node->attr('content');
#			#print "$name - $value \n";
#			print "$name: $value<\n" if( $DEBUG_META );
#			if( $name eq 'og:image' and $date eq '' ) {
#				$date = date_og_image($value);
#			} elsif( $name eq 'og:title' and $title eq '' ) {
#				$title = $value;
#			} elsif( $name eq 'og:description' and $desc eq '' ) {
#				$desc = $value;
#			}
#		}
#	}
# Prep for refactoring. ignore now END

sub date_og_image {
	my ($dateurl) = @_;
	my $retdate = "";
	my $resp;
	my $mech2 = WWW::Mechanize->new(agent => 'og_date');

	$resp = $mech2->get($dateurl);
	$retdate = $mech2->response()->header('last-modified');
	if( not $mech2->success() ) {
		warn $mech2->status() . " - " . $resp->status_line;
		#warn "No og:image found!\n";
		$retdate = "";
	} elsif( not defined $retdate ) {
		$retdate = "";
		#warn "No last modified for og:image\n";
	} else {
		#print "OG IMG DATE: $retdate\n";
	}
	return ($retdate);
}
 
